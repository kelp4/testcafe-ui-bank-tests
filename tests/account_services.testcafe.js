import { Selector } from 'testcafe';

fixture `Account Services`
    .page `https://parabank.parasoft.com/parabank/admin.htm`
    .beforeEach(async t => {
        await t
            .click(Selector('[name="login"]').find('[name="username"]'))
            .typeText(Selector('[name="login"]').find('[name="username"]'), 'test')
            .typeText(Selector('[name="login"]').find('[name="password"]'), 'test')
            .click(Selector('#loginPanel').find('.button'))
            .expect(Selector('.ng-scope').find('div').withText('Accounts Overview').exists).ok();
    });

test('Open New Account', async t => {
    await t
        .click(Selector('a').withText('Open New Account'))
        .click(Selector('#type'))
        .click(Selector('#fromAccountId'))
        .click(Selector('option').withText('16563'))
        .click(Selector('#rightPanel').find('.button'))
        .click(Selector('p').withText('Congratulations, your account is now open.'))
        .expect(Selector('p').withText('Congratulations, your account is now open.').innerText).ok()
        .click(Selector('#newAccountId'));
});

test('Transfer Funds', async t => {
    await t
        .click(Selector('a').withText('Transfer Funds'))
        .click(Selector('#amount'))
        .typeText(Selector('#amount'), '1000')
        .click(Selector('#fromAccountId'))
        .click(Selector('#fromAccountId').find('option').withText('13122'))
        .click(Selector('p').withText('Amount: $'))
        .click(Selector('.ng-valid.ng-dirty.ng-valid-parse').nth(0))
        .click(Selector('#toAccountId'))
        .click(Selector('#toAccountId').find('option').withText('12789'))
        .click(Selector('#rightPanel').find('.button'))
        .expect(Selector('h1').withText('Transfer Complete!').innerText).ok();
});

test('Bill Payment', async t => {
    await t
        .click(Selector('a').withText('Bill Pay'))
        .expect(Selector('h1').withText('Bill Payment Service').innerText).ok()
        .typeText(Selector('.ng-pristine.ng-valid').find('[name="payee.name"]'), 'test')
        .typeText(Selector('.ng-valid.ng-dirty.ng-valid-parse').find('[name="payee.address.street"]'), '123')
        .typeText(Selector('.ng-valid.ng-dirty.ng-valid-parse').find('[name="payee.address.city"]'), 'test')
        .typeText(Selector('.ng-valid.ng-dirty.ng-valid-parse').find('[name="payee.address.state"]'), 'test')
        .typeText(Selector('.ng-valid.ng-dirty.ng-valid-parse').find('[name="payee.address.zipCode"]'), '123')
        .typeText(Selector('.ng-valid.ng-dirty.ng-valid-parse').find('[name="payee.phoneNumber"]'), '123')
        .typeText(Selector('.ng-valid.ng-dirty.ng-valid-parse').find('[name="payee.accountNumber"]'), '123')
        .typeText(Selector('.ng-valid.ng-dirty.ng-valid-parse').find('[name="verifyAccount"]'), '123')
        .typeText(Selector('.ng-valid.ng-dirty.ng-valid-parse').find('[name="amount"]'), '10')
        .click(Selector('#rightPanel').find('.button'))
        .expect(Selector('h1').withText('Bill Payment Complete').innerText).ok();
});

test('Apply for Loan', async t => {
    await t
        .click(Selector('a').withText('Request Loan'))
        .typeText(Selector('#amount'), '500')
        .typeText(Selector('#downPayment'), '10')
        .click(Selector('#rightPanel').find('.button'))
        .expect(Selector('h1').withText('Loan Request Processed').innerText).ok()
        .expect(Selector('b').withText('Your new account number:').innerText).ok();
});
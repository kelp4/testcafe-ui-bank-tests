# ParaBank UI Test Automation 

Web-app automation fpr [ParaBank](https://parabank.parasoft.com/parabank/index.htm) using [TestCafe](https://devexpress.github.io/testcafe/)

  - :warning:The ParaBank test  website is flaky; logins may not always work



## After Cloning Repo
CD to the repo folder and:

    npm install


## To execute the tests locally

    npm test